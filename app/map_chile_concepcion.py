
import folium
#import branca
#import dbConnect as conn

def generate_map():
    map = folium.Map(location=(-36.8260216,-73.1030856), 
                    zoom_start=12,
                    width='70%', 
                    height='70%',left='20%', top='5%', position='relative',
                    control_scale=True, 
                    zoom_control=True)    
    
    map.save("map_chile_test.html")

if __name__ == '__main__':
    generate_map()